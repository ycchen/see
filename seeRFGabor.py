from __future__ import division
import math
import numpy as np
import matplotlib.cm as cm
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
from seeRF import *
import sys
import cv2

class seeRFGabor(seeRF):
    """
       this class represents a gabor receptive field RF

       Instance variables:
       -------------------
       :N: 
       :center:
       :radius:
       :theta:
       :inVtxIdx:
       :coeffs:
       :inSignal:
       :inGraph:
    """

    def __init__(self, center, radius, inVtxIdx, inVtxDist, theta, inGraph):
        """
           parameters:
           -----------
           :center: array_like
                    the (x,y) euclidean coordinates of the RF.
           :radius: float
                    the radius of the RF in visual angles.
           :inVtxIdx: array_like (numpy)
                      an array that holds vertex indexes in inGraph that are inputs to the current RF              
           :inVtxDist: array_like (numpy)
                      an array that holds the distance of each input vertex in inVtxIdx to the center
                      of the RF (in visual angles).
           :theta: float
                   the orientation of the gabor kernel
           :inGraph: seeGraph   
                     the graph on which to apply the RF.


        """
     
        self.radius = radius
        self.center = center
        self.inVtxIdx = inVtxIdx
        self.inVtxDist = inVtxDist
        self.inGraph = inGraph
        self.theta = theta

        #Number of input vertices
        self.N = len( self.inVtxIdx )

        #the filter coefficients at this vertex
        self.coeffs = []


        for i in np.arange( self.N ):

            #get filter value at this vertex
            coeff = self.getCoeff(self.inGraph.coords[ self.inVtxIdx[i] ] - self.center)

            #add value to the self.coeffs list
            self.coeffs.append(coeff)


        #this division might cause a division by zero when the RF does not find
        #vertices in its neighborhood
        #in this case
        if self.N != 0:
            #force the integral of the filter coefficients to be zeros
            self.coeffs = self.coeffs - np.sum(self.coeffs) / self.N

            #L2-normalize the vector of coefficients
            self.coeffs = cv2.normalize(self.coeffs)

            #reshape into 1D array
            self.coeffs = self.coeffs.reshape( len(self.coeffs) )




    def captureInput(self):
        """
           this function captures its input signal from inGraph.signals
           this function create the instance variable self.inSignal


        """


        #take the inGraphsignal values at self.inVtxIdx
        self.inSignal = self.inGraph.signals[self.inVtxIdx]

        #if the captured signal is an RGB signal, take the gray level version of it
        self.inSignal = np.mean( self.inSignal, axis = 1 )



    def fire(self):
        """
           this function performs an inner product between its coefficients 
           and its input signal (self.coeffs * self.inSignal) and returns the results.

           returns:
           --------
           the result of the inner product between self.coeffs and self.inSignal
        """
        
        return np.inner(self.coeffs, cv2.normalize(self.inSignal).reshape(len(self.coeffs)))


    def getCoeff(self, coord):
        """
           this function implements the equation that computes the RF filter coefficients
           at a point (x, y) relative to the center of the RF, the x is the classic left-to-right
           horizontal axis. y is the classic bottom-to-top vertical axis.
 
           parameters:
           -----------
           :coord: float
                   the (x,y) coordinate relative to the center of the RF.

           returns:
           --------
           the coefficient value at the point described by (x,y).

           important:
           ----------
           The calculation of the parameters of the gabor filter in this function is inspired by:

           Serre, T., Wolf, L., Bileschi, S., Riesenhuber, M., & Poggio, T. (2007). Robust object recognition with cortex-like mechanisms. Pattern Analysis and Machine Intelligence, IEEE Transactions on, 29(3), 411-426.
        """

        
        #setting the parameters of the center-surround kernel as in 
        a = 2.5 # a = RFdiameter/sigma. this is approximated from thomas serre paper on Hmax. 
                # where sigma is the standard deviation of the gaussian part of the gabor filter.

        b = 0.8 # b = sigma/lambda. where lambda is the wavelength of the cosine part

        gamma = 0.3 # the aspect ratio of the gabor filter adopt from thomas serre paper on Hmax.

        
        #now calculate the values of sigma, lambda
        sigma = (self.radius * 2 ) / a 
        lmbda = sigma / b

        #convert theta to radians
        theta = math.radians(self.theta)

        #begin the apply the gabor equation
        x,y = coord

        x0 = x * np.cos(theta) + y * np.sin(theta)

        y0 = - x * np.sin(theta) + y * np.cos(theta)


        #the cosine part
        cos_prt = np.cos( (2 * np.pi * x0) / lmbda) 

        #the gaussian part
        gauss_prt = np.exp( - (x0**2 + gamma**2 * y0**2) / (2 * sigma**2) )

        return  cos_prt * gauss_prt


    def drawRough(self):
        """
           this is a function that roughly plots the RF on a 2D plane
           'may be deleted later'


           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           
           important:
           ----------
           due to poor implementation, the RF should by entirely located in a plane
           quarter in order to be plotted


        """
        coords = np.empty((len(self.inVtxIdx),2), dtype = np.int32)

        for i in np.arange(len(self.inVtxIdx)):
            coords[i,:] = (self.inGraph.projCoords[self.inVtxIdx[i],:])

        mxH = np.max(coords[:,0])
        mnH = np.min(coords[:,0])
        mxW = np.max(coords[:,1])
        mnW = np.min(coords[:,1])



        kernel = np.zeros((np.abs(mxH - mnH) , np.abs(mxW - mnW) ), dtype = np.float32)

        for i in np.arange(len(self.coeffs)):
            kernel[np.abs(coords[i,0] - mnH -1), np.abs(coords[i,1] - mnW - 1)] = self.coeffs[i]




        plt.imshow(kernel, interpolation = 'nearest')
        plt.colorbar()

        plt.show()



    def draw(self, show = True):
        """
           
           parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() to show the figure on the screen.
                  If False, the figure is created but not shown.

           returns:
           --------
           :fig: object returned by matplotlib.pyplot.figure()
                 this object allows the figure to return and manipulated later in the calling function.
        """

        fig = plt.figure()

        #array of vertex colors
        vcolor = np.copy(self.coeffs)
 
        #normalize the values between 0 and 1
        vcolor = (vcolor - np.min(vcolor) ) / (np.max(vcolor) - np.min(vcolor) )

        for i in np.arange(self.N):
            fig.gca().add_patch(plt.Circle(self.inGraph.coords[self.inVtxIdx[i]], 0.0005, fc = [vcolor[i]]*3, color = [vcolor[i]]*3))


        fig.gca().axis('off')
        fig.gca().axis('scaled')

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()

        return fig




    def __str__(self):
        pass

