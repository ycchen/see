from __future__ import division
import math
import numpy as np
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
import sys
from seeGraphRetina import *
from seeImage import *
from seeSystem import *
from seeRFCenterSurround import *
from seeRFGabor import *
from seeGraphCortex import *
from seeSaliency import *
import cPickle
import os
from pathos.multiprocessing import Pool
import shutil
import argparse
import logging



#instantiate a logging object
logFile = os.path.join( os.path.dirname(__file__), "log/exceptions.log")
logging.basicConfig(filename = logFile, level = logging.DEBUG)


def runSaliencyBenchmark():
    """
       this function runs the saliency model in seeSaliency on the benchmark in 'destination'

    """


    #getting necessary global variables (this step is not necessary, I do this just for a better readablity)
    source = globals()["source"]
    destination = globals()["destination"]
    imageCount = globals()["imageCount"] 
    fixCount = globals()["fixCount"] 
    inhibDiam = globals()["inhibDiam"] 
    procount = globals()["procount"] 
    focusCenter = globals()["focusCenter"]
    imgAng =  globals()["imgAng"]    

    #instantiate a seeSystem object
    sSystem = seeSystem()

    #build a seeSystem (uncomment the following line if this is your first run or you need to rebuild the system
    #sSystem.build()


    #if 'destination' folder already exists, delete it
    if os.path.exists(destination):
        shutil.rmtree(destination)

    #create the destination directory
    os.mkdir(destination)

    #create directories to store fixated images and fixation maps
    fixatedImagePath = os.path.join(destination, 'fixatedImages')
    fixationMapPath = os.path.join(destination, 'fixationMaps')
    saliencyMapPath = os.path.join(destination, 'SaliencyMaps')

    os.mkdir(fixatedImagePath)
    os.mkdir(fixationMapPath)
    os.mkdir(saliencyMapPath)

    #create a pool for multiprocessing using pathos.multiprocessing library
    pool = Pool()

    #number of available processes. leave only one processor unused
    proc_count = len(pool._pool) 

    if procount > 0 and \
       procount < proc_count:

        proc_count = procount

    #get the number of images in the benchmark source path
    if imageCount == None:
        imageCount = len(os.listdir(source))

    #run multiple processes to generate fixation maps and fixated images on
    #the benchmark

    for i in np.arange(proc_count):

        #start a process
        pool.apply_async(_proc_runSaliencyBenchmark, \
                         args = ( fixatedImagePath, \
                                  fixationMapPath, \
                                  saliencyMapPath,\
                                  (imageCount * i) // proc_count, \
                                  (imageCount * (i+1) ) // proc_count
                          ))

    pool.close()
    pool.join()


def _proc_runSaliencyBenchmark(fixatedImagePath, fixationMapPath, saliencyMapPath, startIdx, endIdx):
    """
       this is a function meant to run as a process in a multiprocess operation
       to compute a part of fixation maps and fixated image on a benchmark in 'source'

       Parameters:
       -----------
       :fixatedImagePath: string
                          the path to which to save fixated images
       :fixationMapPath: string
                         the path to which to save fixation maps
       :saliencyMapPath: string
                         the path to which to save saliency maps
       :startIdx: integer
                  the index of the first image in 'source'
       :endIdx: integer
                the index of the image following the final image to analyse.

        Returns:
        --------
        No values is return, calculated images are stored in 'destination'
    """
    
    #instantiate a seeSystem object
    sSystem = seeSystem()

    #load a seeSystem
    sSystem.load()

    #get the list of image names in 'source'
    images = os.listdir(source)

    logging.info(str(images[startIdx:endIdx]))

    #begin looping to analyse each image in 'source'
    for i in np.arange(startIdx, endIdx):



        try:        

            #do this only for files and not for folders
            if os.path.isfile( os.path.join(source, images[i]) ):

                #create the seeImage
                sImage = seeImage(path = os.path.join(source, images[i]), \
                                  diagAng = imgAng)

                #sample the seeImage
                sSystem.sGraphRetina.sampleImage( sImage, focusCenter = focusCenter )

                #capture the RF resulting signals
                sSystem.sGraphCortex.captureSignal()

                #create a seeSaliency object
                sSaliency = seeSaliency(sImage, \
                                        sSystem.sGraphCortex, \
                                        fixCount = fixCount, \
                                        inhibDiam = inhibDiam)

                sSaliency.getFixatedImage(save = True, \
                                          savePath = fixatedImagePath)

                sSaliency.getFixationMap(save = True, \
                                         savePath = fixationMapPath)

                sSaliency.getSaliencyMap(save = True, \
                                         savePath = saliencyMapPath)


        except Exception, e:

            logging.exception(str(e))


if __name__ == "__main__":

    #getting command-line arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("-s", "--source", \
                        required = True, \
                        help = "The path to the benchmark's image folder")

    parser.add_argument("-d", "--destination", \
                        required = True, \
                        help = "The path to the destination folder where results are to be stored")

    parser.add_argument("-c", "--imageCount", \
                        type = int, \
                        help = "the number of images to process in the source folder")

    parser.add_argument("-f", "--fixCount", \
                        type = int, \
                        default = 50, \
                        help = "the number of fixations to extract")

    parser.add_argument("-i", "--inhibDiam", \
                        type = float, \
                        default = "1", \
                        help = "the diameter (in visual angles) of the IOR area")

    parser.add_argument("-o", "--focusCenter", \
                        type = int, \
                        default = [0,0], \
                        nargs = '+', \
                        help = "the coordinated of the center of gaze point.Example 0 0 ")

    parser.add_argument("-p", "--procount", \
                        type = int, \
                        default = 0, \
                        help = "the number of processors to be used for calculation, type 0 to used all available processors")

    parser.add_argument("-g", "--imgAng", \
                        type = float, \
                        default = 30, \
                        help = "the visual angle to be occupied by the diagonal of the image")

    args = parser.parse_args()

    #add the command-line arguements to the global namespace
    globals().update(args.__dict__)

    #begin benchmarking
    runSaliencyBenchmark()
