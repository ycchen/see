from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.cm as cm
from itertools import product
from math import *
import cv2
import os
import shutil

#directory name

"""
dir_names = ["Random", "Indoor", "Action", "Art", "LowResolution",\
            "LineDrawing", "Noisy", "Object", "Social", "OutdoorNatural",\
            "OutdoorManMade", "Satelite", "Pattern", "Sketch", "Jumbled",\
            "Inverted", "Cartoon", "Affective", "BlackWhite", "Fractal"]
"""

source_dir = "" 



for name in dir_names:

    #path of fixation points maps
    fxpts_dir = os.path.join( os.path.dirname(__file__), source_dir, "fixationMaps")
    fxmap_dir = os.path.join( os.path.dirname(__file__) , source_dir, "saliencyMaps62")
    print fxpts_dir, fxmap_dir


    #do only if the source directory exists
    if  os.path.exists(fxpts_dir):

        if  os.path.exists(fxmap_dir):
            shutil.rmtree(fxmap_dir)

        os.mkdir(fxmap_dir)

        #looping through images
        for fname in os.listdir(fxpts_dir):


            #loading a binary fixation map
            fxpts =  mpimg.imread(os.path.join(fxpts_dir, fname) )

            #converting it to a float32 image
            fxpts = fxpts.astype(np.float32)

            #normalizing its values to [0,1]
            fxpts = fxpts / np.amax(fxpts)

            #convolving with the gaussian kernel
            fxMap = cv2.GaussianBlur(fxpts, (451, 451) , 62)

            #normalizing to [0,255]
            fxMap = (fxMap / np.amax(fxMap)) * 255


            #saving on disk
            cv2.imwrite( os.path.join( fxmap_dir, fname ), fxMap)


