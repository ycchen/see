from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import euclidean
import random

class seeGraph(object):
    """A class representing a graph"""

    #a static variable that gives the number of instantiated graphs
    level_count = 0

    def __init__(self):


        #increment the the number of levels
        seeGraph.level_count += 1

        self.type = "none"





    def setLevel(self, level):
        """
           sets the level of the graph.
        """
   
        self.level = level




    def setOrder(self, order):
        """
           sets the order of the graph within its level. {0,...,M}. Example: in the level 0 we can have an order 0 graph 
           for the R component of and RGB image, order 1 for the G component and 2 for the B component.
        """
   
        self.order = order



    def computeProjCoords(self, dist, coords):
        """
           This function computes the coordinates of each coordinate vector in 'coords' when projected
           on a flat surface ( the image ) put on a given distance from that graph.
           The reason why this is necessary to compute is that coords is in visual angles,
           while the coordinates to computes using this function are in pixels for examples.

           parameters:
           -----------

           :dist: float
                  the distance between the center of the graph and the surface (the image)
           :coords: array-like (nx2)
                    this is an array of the coordinated to convert into projection coordinates


           returns:
           --------
           :projCoords: the the coordinates  on the projection surface (image)

        """
        
        #just before beginning, I will treat the case when coords has only one coordinate in the form of 
        #one dimensional array of size 2
        coords = np.array(coords) #first convert to numpy array

        if len(coords.shape) == 1 and len(coords == 2): #test if coords is a 1D array of size 2
            coords = coords.reshape(1,2)

        #declare the projection coordinates array as euclidean coordinates
        projCoords = np.zeros( (len(coords), 2), dtype = np.float32)

        for i in np.arange(len(coords)):
 
            #compute the polar radius coordinate of the vertex ( which is computed in visual angle degrees)
            #this is used as the visual angle in computing the projection coordinates
            rad_ang = euclidean((0,0), coords[i,:])

            #convert rad_ang to radians
            rad_ang = (rad_ang * np.pi) / 180 

            #compute the polar angle coordinate of the vertex (radians)
            angle = np.arctan2(coords[i,1], coords[i,0])


            #compute the projection polar radius in the same units as the input parameter dist (most likely in pixels)
            proj_rad = dist * np.tan(rad_ang)

            #compute the projection euclidean coordinates and round them to the closest integer
            projCoords[i,0] = np.round( np.cos(angle) * proj_rad )
            projCoords[i,1] = np.round( np.sin(angle) * proj_rad )

        if len(projCoords) == 1 : #test if projCoords contains only one coordinate
            return projCoords[0] #return the result as a 1D array

        else:
            return projCoords





    def show(self):
        """plots the graph in 2D"""

        plt.axes()

        #drawing vertices
        for i in np.arange(self.N):


            self.coords[i,0] = self.coords[i,0] + random.random()
            self.coords[i,1] = self.coords[i,1] + random.random()

            plt.gca().add_patch(plt.Circle(self.coords[i,:], 0.1 , fc = 'y'))

        #drawing edges
        for i in np.arange(self.N):
            for j in np.arange(i):
                if self.A[i,j] == True:
                    plt.gca().add_line(plt.Line2D( self.coords[(i,j),0], self.coords[(i,j),1], lw = 1, zorder = 0))


               

        plt.axis('off')
        plt.axis('scaled')
        plt.show()



    def __str__(self):
        """display graph information"""

        output_str = ""

        if hasattr(self, 'type'):
            output_str = output_str + "\n\tType: " + str(self.type)

        if hasattr(self, 'level'):   
            output_str = output_str + "\n\tlevel = " + str(self.level)

        if hasattr(self, 'N'):
            output_str = output_str + "\n\tN = " + str(self.N)

        if hasattr(self, 'A'):
            output_str = output_str + "\n\tA: [" + str(self.A.shape[0]) + " x "  + str(self.A.shape[1]) + "] " + str(type(self.A[0,0]))

        if hasattr(self, 'W'):
            output_str = output_str + "\n\tW: [" + str(self.W.shape[0]) + " x "  + str(self.W.shape[1]) + "] " + str(type(self.W[0,0]))

        if hasattr(self, 'coords'):
            output_str = output_str + "\n\tcoords: [" + str(self.coords.shape[0]) + " x "  + str(self.coords.shape[1]) + "] " + str(type(self.coords[0,0]))

        return output_str



