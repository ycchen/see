from __future__ import division
import math
import numpy as np
import scipy.sparse 
from scipy.spatial.distance import euclidean
from seeGraph import *
import sys

class seeRF:
    """
       this class represents a receptive field RF
    """

