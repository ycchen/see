from __future__ import division
import numpy as np
import scipy.spatial.distance as pdist
import matplotlib.pyplot as plt
import cv2
import os
import matplotlib.cm as cm
from itertools import product
from math import *
import shutil

class seeSaliency:
    """
       this is a class that provides some useful methods related to the creation of saliency maps using seeSystem
       data.

       Member Variables:
       -----------------
       :sImage:
       :sGraphRetina:
       :sGraphCortex:
       :fixCount:
       :inhibDiam:
       :conspicuityMap:
       :fixationPoints:
       :fixationMap:
       :fixatedImage:
    """

    def __init__(self, sImage, sGraphCortex, fixCount, inhibDiam = None):
        """
           Parameters:
           -----------
           :sImage: seeImage
           :sGraphCortex: seeGraphCortex
                          this graph contains the signals on which to operates to produce saliency maps.
           :fixCount: integer
                      the number of fixations to extract.
           :inhihDiam: float
                       the diameter (in visual angles) of the inhibition of return region.
                       if the value is None, it takes the value of fovAng in sGraphCortex.inGraph

        """ 

        self.sImage = sImage
        self.sGraphCortex = sGraphCortex
        self.fixCount = fixCount

        self.sGraphRetina = sGraphCortex.inGraph

        #setting the value of self.inhibDiam
        if inhibDiam == None:
            self.inhibDiam = self.sGraphRetina.fovAng
        else:
            self.inhibDiam = inhibDiam
        

    @staticmethod
    def ittiNormalize(signals):
        """
           this function applies the normalization method of conspicuity maps presented by Laurent Itti in

           ##Itti, L., Koch, C., & Niebur, E. (1998). 
           ##A model of saliency-based visual attention for rapid scene analysis. 
           ##IEEE Transactions on pattern analysis and machine intelligence, 20(11), 1254-1259.
        

           parameters:
           -----------
           :signals: 2D array
                     this an array of signals representing feature maps. each row is a vector representing on
                     map.
 

           returns:
           --------
           an array of the same shape corresponding the Itti-normalized signals.

        """

        #setting negative values to zeros (half-wave rectification)
        signals = np.where( signals >= 0, signals, 0 )


        #the case when signals contains multiples lines
        if len(signals.shape) > 1:

            #normalizing values to the range [0,1]            
            sigMax = np.amax(signals, axis = 1)
        
            sigMax = np.where(sigMax == 0, 1, sigMax) #substitute 0 max values with
                                                      #ones to avoid zeros division

            sigMin = np.amin(signals, axis = 1)


            #[0,1] normalization
            signals = (signals.T - sigMin ) / (sigMax - sigMin)             

            #retransposing the signals which was transposed in the previous lines
            signals = signals.T

            

            #a multiplicative factor as used in Itti's paper, here I change the definition
            m_factor = ( np.amax(signals, axis = 1) - \
                         np.mean(signals, axis = 1) ) ** 2


            #application of the m_factor to enhance saliency
            signals = signals.T * m_factor


            #retransposing the signals which was transposed in the previous lines
            signals = signals.T



        #repeat previous operations for the case where signals contains only
        #one line     
        elif len(signals.shape) == 1:
            sigMax = np.amax(signals)
            sigMax = 1 if sigMax == 0 else sigMax
            sigMin = np.amin(signals)
            signals = (signals - sigMin) / (sigMax - sigMin)
            m_factor = ( np.amax(signals) - np.mean(signals) ) ** 2
            signals = signals * m_factor


        return signals



    def getGlobalConspicuityMap(self):
        """
           this function is responsible of combining the mutliple feature maps (self.sGraphCortex.signals)
           into one global conspicuity map self.conspicuityMap

        """

        #clone the sGraphCortex.signals variable
        #notice that we correctly use the np.copy() method to copy this 2D list of arrays
        #because all arrays have the same size within the list to the np.copy returns a 2D numpy array. 
        signals = np.copy(self.sGraphCortex.signals)


        #normalizing all signals. Itti normalization
        signals = seeSaliency.ittiNormalize(signals)


        #create the conspicuity map as a simple average of all normalized feature maps
        self.conspicuityMap = np.sum(signals, axis = 0) / signals.shape[0]


    def _getFixationPoints(self):
        """
           this function returns seeImage coordinates (classic image pixel coordinates) 
           corresponding to the self.fixCount most salient places in the image
        """

        #if self.conspicuityMap does not exist, create it
        if not hasattr(self, 'conspicuityMap'):
            self.getGlobalConspicuityMap()

        #take a copy of self.conspicuity map
        conspicuityMap = np.copy(self.conspicuityMap)

        #initialize a member variable to hold the fixation points
        self.fixationPoints = list()

        #iterate self.fixCount times
        for i in np.arange(self.fixCount):

            #take the index of the most salient vertex in the conspicuity map
            fixIdx = np.argmax( conspicuityMap )

            #get the projection coordinates corresponding to the most salient vertx
            fixProjCoords = self.sGraphCortex.computeProjCoords( \
                                     self.sImage.dist, \
                                     self.sGraphCortex.coords[fixIdx] + self.sGraphRetina.focusCenter )

            #finally, get the corresponding pixel indexes 
            #(vertical, horizontal) of the image
            fixImageCoords = self.sImage.getPixIdx(fixProjCoords)

            #add the fixation point to self.fixPtx
            self.fixationPoints.append( fixImageCoords )

            #apply the inhibition of return around that salient location
            #in the conspicuity map
            conspicuityMap = self._IOR( conspicuityMap, idx = fixIdx )



    def _IOR(self, conspicuityMap, idx):
        """
           The Inhibition of Return (IOR) function
           this function applies an inhibition area around the most salient 
           value referred to by idx. The size of this area is defined in 
           self.inhibDiam

           Parameters:
           -----------
           :conspicuityMap: 1D numpy array
                            the signal representing the global conspicuity map
           :idx: integer
                 the index in 'conspicuityMap' corresponding to the most salient location
                 around which to apply the inhibition zone

           returns:
           --------
           the conspicuity map on which the IOR is applied.
        """


        #a list of indexes in conspicuityMap to be inhibited
        inhibIdx = list()

        #get the coordinate of the center of the IOR area
        center = self.sGraphCortex.coords[idx]

        #get the squared radius of the inhibition zone
        #I use the squared radius instead of the radius because later
        #I will compare with the squared euclidean distance.
        radius = self.inhibDiam / 2
        rad2 = radius**2


        #find the vertices that fall within the IOR zone
        for i in np.arange( self.sGraphCortex.N ):

            #take the vertex coordinates
            vertex = self.sGraphCortex.coords[i]

            #the following conditions limits the search of vertices to within
            #a square of side size 'self.inhibDiam' centered at 'center'
            if np.abs(vertex[0] - center[0] ) <= radius \
               and \
               np.abs(vertex[1] - center[1] ) <= radius:

            
                #if the above condition is met, then the vertex is within 
                #a square around the inhibition center. No we should compare 
                #distances in order to see whether the point is within the 
                #inhibiton circle.
                d2 = pdist.sqeuclidean(center, vertex)

                #if the point is within the inhib zone add the index to inhibIdx
                if d2 <= rad2:
                    inhibIdx.append(i)


        #apply the inhibtion zone
        conspicuityMap[inhibIdx] = 0

        return conspicuityMap


    def drawFixatedImage(self, show = False):
        """
           this function draw fixation points on the original image

           Parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() 
        """

        fig = plt.figure()

        #check whether self.fixatedImage is already created. If not create it
        if not hasattr(self, 'fixatedImage'):
            self.getFixatedImage()


        fig.gca().imshow(self.fixatedImage)

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()


        return fig


    def getFixatedImage(self, save = False, savePath = None, saveName = None):
        """
           this function superimposes fixation points onto a copy of sImage.image

           Parameters:
           -----------
           :save: boolean
                  when True, the fixated images is saved to 'savePath'
           :savePath: string
                      the path to which to save the fixated image when 'save' is True
           :saveName: string (filename.imageExtention)
                      this is the image filename under which the fixated image is to be saved


           Returns:
           --------
           this function does not return any value, it just creates the self.fixatedImage variable
        """

        #skip if self.fixatedImage already exists
        if not hasattr(self, 'fixatedImage'):

            #take a copy of self.sImage.image
            self.fixatedImage = np.copy(self.sImage.image)

            #check whether self.fixationPoints is already created. If not create it
            if not hasattr(self, 'fixationPoints'):
                self._getFixationPoints()

            #draw circles on fixation points
            for pt in self.fixationPoints:
                cv2.circle( self.fixatedImage, (pt[1],pt[0]), 5, (1,0,0), thickness = 2)
        

        #save the file if 'save' is True
        if save == True:

            if savePath == None:
                print "seeError: argument 'savePath' \
                       to seeSaliency.setFixatedImage() is none"
                return

            elif saveName == None:
                saveName = self.sImage.filename

            #save the fixated image on disk
            #the images should be stored with pixel values in [0,255]
            #open-cv saves image in BGR mode, so I reconvert it to RGB
            cv2.imwrite(os.path.join(savePath, saveName), \
                        self.fixatedImage[:,:,::-1] * 255 )



    def getFixationMap(self, save = False, savePath = None, saveName = None):
        """
           this function creates a fixation map. A fixation map is a binary and image of
           the same height and width as self.sImage.image. The fixation points are
           represented as white points on a black background.

           Parameters:
           -----------
           :save: boolean
                  when True, the fixation map is saved to 'savePath'
           :savePath: string
                      the path to which to save the fixation map when 'save' is True
           :saveName: string (filename.imageExtention)
                      this is the image filename under which the fixation map is to be saved


           Returns:
           --------
           this function does not return any value, it just creates the self.fixationMap variable

        """

        #skip if self.fixationMap already exists
        if not hasattr(self, 'fixationMap'):

            #initialize the fixation map
            self.fixationMap = np.zeros( (self.sImage.height, self.sImage.width),
                                      dtype = np.float32)

            #check whether self.fixationPoints is already created. If not create it
            if not hasattr(self, 'fixationPoints'):
               self._getFixationPoints()


            #fixation points are represented by white pixels
            self.fixationMap[zip(*self.fixationPoints)] = 1.0




        #save the file if 'save' is True
        if save == True:

            if savePath == None:
                print "seeError: argument 'savePath' \
                       to seeSaliency.setFixationPoints() is none"

            elif saveName == None:
                saveName = self.sImage.filename


            #save the fixation map on disk
            #the images should be stored with pixel values in [0,255]
            cv2.imwrite(os.path.join(savePath, saveName), \
                        self.fixationMap * 255 )



    def drawFixationMap(self, show = False):
        """
           this function draw fixation points as white points on a black background

           Parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() 
        """

        fig = plt.figure()

        #check whether self.fixationMap is already created. If not create it
        if not hasattr(self, 'fixationMap'):
            self.getFixationMap()


        fig.gca().imshow(self.fixationMap, \
                         cmap = cm.Greys_r)

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()


        return fig


    def getSaliencyMap(self, save = False, savePath = None, saveName = None):
        """
           this function create a saliency map that has the same shape as the original seeImage. 
           A saliency map is a continuous grayscale image composed of a convolution between a  fixation map
           and a Gaussian kernel.

           Parameters:
           -----------
           :save: boolean
                  when True, the saliency map is saved to 'savePath'
           :savePath: string
                      the path to which to save the saliency map when 'save' is True
           :saveName: string (filename.imageExtention)
                      this is the image filename under which the sliency map is to be saved


           Returns:
           --------
           this function does not return any value, it just creates the self.saliencyMap variable

        """

        #skip if self.saliencyMap already exists
        if not hasattr(self, 'saliencyMap'):

            #initialize the saliency map
            self.saliencyMap = np.zeros( (self.sImage.height, self.sImage.width),
                                      dtype = np.float32)

            #check whether self.fixationMap is already created. If not create it
            if not hasattr(self, 'fixationMap'):
               self.getFixationMap()


            #convolving with the gaussian kernel
            self.saliencyMap = cv2.GaussianBlur(self.fixationMap, (451, 451) , 62)

            #normalizing to [0,1]
            self.saliencyMap = (self.saliencyMap - np.amin(self.saliencyMap)) / (np.amax(self.saliencyMap) - np.amin(self.saliencyMap))



        #save the file if 'save' is True
        if save == True:

            if savePath == None:
                print "seeError: argument 'savePath' \
                       to seeSaliency.setFixationPoints() is none"

            elif saveName == None:
                saveName = self.sImage.filename


            #save the saliency map on disk
            #the images should be stored with pixel values in [0,255]
            cv2.imwrite(os.path.join(savePath, saveName), \
                        self.saliencyMap * 255 )


    def drawSaliencyMap(self, show = False):
        """
           this function draw the saliency map

           Parameters:
           -----------
           :show: boolean
                  this is just a flag, when True, the function uses plt.show() 
        """

        fig = plt.figure()

        #check whether self.fixationMap is already created. If not create it
        if not hasattr(self, 'saliencyMap'):
            self.getSaliencyMap()


        fig.gca().imshow(self.saliencyMap, \
                         cmap = cm.Greys_r)

        #show the figure if the 'show' flag is True
        if show == True:
            plt.show()


        return fig









